package revolut.testask.customer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CustomerDao {
    private Map<String, Customer> customers = new HashMap<>();

//    public Iterable<String> getCustomersList() {
    public Collection<Customer> getCustomersList() {
//        return customers.stream().map(Customer::getUsername).collect(Collectors.toList());
//        customers.put("123", new Customer("qwe", "asd", "zxc"));
        return customers.values();
    }

    public Customer addCustomer(Customer customer) {
//        Customer customer = new Customer(firstName, lastName, account);
        customers.put(customer.getCustomerId(), customer);

        return customer;
    }

    public Customer getCustomer(String customerId) {
        return customers.get(customerId);
    }
}
