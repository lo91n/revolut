package revolut.testask.customer;

import java.util.UUID;

public class Customer {
    private String customerId;
    private String firstName;
    private String lastName;
    private String account;

    public Customer(String firstName, String lastName, String account) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.account = account;
        this.customerId = getCustomerId();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCustomerId() {
        if (this.customerId == null ) {
            setCustomerId();
        }

        return customerId;
    }

    public void setCustomerId() {
        this.customerId = UUID.randomUUID().toString();
    }
}
