package revolut.testask.customer;

import com.google.gson.Gson;
import spark.Request;
import spark.Response;
import spark.Route;

import static revolut.testask.Application.customerDao;
import static revolut.testask.util.JsonUtil.dataToJson;
import static revolut.testask.util.RequestUtil.getParamAccountCustomerId;
import static revolut.testask.util.RequestUtil.getParamCustomerId;

public class CustomerController {
    public static Route getCustomersList = (Request request, Response response) -> {
        return dataToJson(customerDao.getCustomersList());
    };

    public static Route addCustomer = (Request request, Response response) -> {
        Customer customer = new Gson().fromJson(request.body(), Customer.class);
        customer.setCustomerId();

        return dataToJson(customerDao.addCustomer(customer)); // TODO replaced on success code or something...
    };

    public static Route getCustomer = (Request request, Response response) -> {
        return dataToJson(customerDao.getCustomer(getParamAccountCustomerId(request)));
    };
}
