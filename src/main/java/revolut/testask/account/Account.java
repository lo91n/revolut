package revolut.testask.account;

public class Account {
    private String accountCustomerId;
    private Double amount;

    public Account(String accountCustomerId, Double amount) {
        this.accountCustomerId = accountCustomerId;
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAccountCustomerId() {
        return accountCustomerId;
    }
}
