package revolut.testask.account;

import revolut.testask.account.Account;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class AccountDao {
    private Map<String, Account> accounts = new HashMap<>();

    public Collection<Account> getAccountsList() {
        return accounts.values();
    }

    public Account addAccount(Account account) {
        accounts.put(account.getAccountCustomerId(), account);

        return account;
    }

    public Account getAccount(String customerId) {
        return accounts.get(customerId);
    }
}
