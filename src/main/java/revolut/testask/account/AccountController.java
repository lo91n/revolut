package revolut.testask.account;

import com.google.gson.Gson;
import spark.Request;
import spark.Response;
import spark.Route;

import static revolut.testask.Application.accountDao;
import static revolut.testask.util.JsonUtil.dataToJson;
import static revolut.testask.util.RequestUtil.getParamCustomerId;

public class AccountController {
    public static Route getAccountsList = (Request request, Response response) -> {
        return dataToJson(accountDao.getAccountsList());
    };

    public static Route addAccount = (Request request, Response response) -> {
        Account account = new Gson().fromJson(request.body(), Account.class);        

        return dataToJson(accountDao.addAccount(account)); // TODO replaced on success code or something...
    };

    public static Route getAccount = (Request request, Response response) -> {
        return dataToJson(accountDao.getAccount(getParamCustomerId(request)));
    };
}
