package revolut.testask;

import static spark.Spark.*;

import revolut.testask.account.AccountController;
import revolut.testask.account.AccountDao;
import revolut.testask.customer.CustomerController;
import revolut.testask.customer.CustomerDao;
import revolut.testask.util.Filters;
import revolut.testask.util.Path;

public class Application {
    public static CustomerDao customerDao;
    public static AccountDao accountDao;

    public static void main(String[] args) {
        customerDao = new CustomerDao();
        accountDao = new AccountDao();

        before("*", Filters.addTrailingSlashes);

        get(Path.Web.CUSTOMERS, CustomerController.getCustomersList);
        get(Path.Web.CUSTOMER, CustomerController.getCustomer);
        post(Path.Web.CUSTOMERS, CustomerController.addCustomer);

        get(Path.Web.ACCOUNTS, AccountController.getAccountsList);
        get(Path.Web.ACCOUNT, AccountController.getAccount);
        post(Path.Web.ACCOUNTS, AccountController.addAccount);

        after("*",                   Filters.addGzipHeader);
    }
}
