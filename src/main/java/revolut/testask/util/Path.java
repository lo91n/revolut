package revolut.testask.util;

public class Path {
    public static class Web {
        public static final String INDEX = "/index/";
        public static final String CUSTOMERS = "/customers/";
        public static final String CUSTOMER = "/customers/:customerId/";
        public static final String ACCOUNT = "/accounts/:customerId/";
        public static final String ACCOUNTS = "/accounts/";

        public static String getACCOUNT() {
            return ACCOUNT;
        }
        public static String getACCOUNTS() {
            return ACCOUNTS;
        }
        public static String getINDEX() {
            return INDEX;
        }
        public static String getCustomers() {
            return CUSTOMERS;
        }
        public static String getCustomer() {
            return CUSTOMER;
        }
    }
}
